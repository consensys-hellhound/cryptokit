package paillier

import (
	"bytes"
	"fmt"
	"gitlab.com/consensys-hellhound/cryptokit/encoding"
	"gitlab.com/consensys-hellhound/cryptokit/tlv"
	"math/big"
)

const (
	TagPrefix   int = 0x1000
	TagN            = TagPrefix + 1
	TagG            = TagPrefix + 2
	TagNSquared     = TagPrefix + 3
	TagL            = TagPrefix + 4
	TagU            = TagPrefix + 5
)

func NewPublicKeyEncoder() encoding.EncoderDecoder {
	return publicKeyEncoder{}
}

type publicKeyEncoder struct {
}

func NewPrivateKeyEncoder() encoding.EncoderDecoder {
	return privateKeyEncoder{}
}

type privateKeyEncoder struct {
}

func (publicKeyEncoder) Encode(v interface{}) (out []byte, err error) {
	publicKey, ok := v.(PublicKey)
	if !ok {
		err = fmt.Errorf("wrong type")
		return
	}
	tlvList := tlv.NewList()
	tlvList.Add(TagN, publicKey.N.Bytes())
	tlvList.Add(TagG, publicKey.G.Bytes())
	tlvList.Add(TagNSquared, publicKey.NSquared.Bytes())
	buf := new(bytes.Buffer)
	tlvList.Write(buf)
	out = buf.Bytes()
	return
}

func (publicKeyEncoder) Decode(in []byte) (out interface{}, err error) {
	reader := bytes.NewReader(in)
	records, err := tlv.Read(reader)
	if err != nil {
		return
	}
	n, err := records.Get(TagN)
	if err != nil {
		return
	}
	g, err := records.Get(TagG)
	if err != nil {
		return
	}
	nSquared, err := records.Get(TagNSquared)
	if err != nil {
		return
	}

	publicKey := PublicKey{
		N:        BigFromBytes(n.Value()),
		G:        BigFromBytes(g.Value()),
		NSquared: BigFromBytes(nSquared.Value()),
	}
	out = publicKey
	return
}

func (privateKeyEncoder) Encode(v interface{}) (out []byte, err error) {
	privateKey, ok := v.(PrivateKey)
	if !ok {
		err = fmt.Errorf("wrong type")
		return
	}
	tlvList := tlv.NewList()
	tlvList.Add(TagN, privateKey.N.Bytes())
	tlvList.Add(TagG, privateKey.G.Bytes())
	tlvList.Add(TagNSquared, privateKey.NSquared.Bytes())
	tlvList.Add(TagL, privateKey.L.Bytes())
	tlvList.Add(TagU, privateKey.U.Bytes())

	buf := new(bytes.Buffer)
	tlvList.Write(buf)
	out = buf.Bytes()
	return
}

func (privateKeyEncoder) Decode(in []byte) (out interface{}, err error) {
	reader := bytes.NewReader(in)
	records, err := tlv.Read(reader)
	if err != nil {
		return
	}
	n, err := records.Get(TagN)
	if err != nil {
		return
	}
	g, err := records.Get(TagG)
	if err != nil {
		return
	}
	nSquared, err := records.Get(TagNSquared)
	if err != nil {
		return
	}
	l, err := records.Get(TagU)
	if err != nil {
		return
	}
	u, err := records.Get(TagU)
	if err != nil {
		return
	}

	publicKey := PublicKey{
		N:        BigFromBytes(n.Value()),
		G:        BigFromBytes(g.Value()),
		NSquared: BigFromBytes(nSquared.Value()),
	}

	privateKey := PrivateKey{
		PublicKey: publicKey,
		L:         BigFromBytes(l.Value()),
		U:         BigFromBytes(u.Value()),
	}
	out = privateKey
	return
}

func BigFromBytes(in []byte) *big.Int {
	out := new(big.Int)
	out.SetBytes(in)
	return out
}
