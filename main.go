package main

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"gitlab.com/consensys-hellhound/cryptokit/paillier"
	"log"
)

func main() {
	fmt.Println("starting app")
	var err error

	// Generate a 128-bit private key.
	privKey, _ := paillier.GenerateKey(rand.Reader, 128)

	fmt.Println(MustJSON(privKey))

	publicKeyEncoder := paillier.NewPublicKeyEncoder()
	privateKeyEncoder := paillier.NewPrivateKeyEncoder()

	fmt.Println("encode public key")
	publicKeyBytes, err := publicKeyEncoder.Encode(privKey.PublicKey)
	if err != nil{
		log.Fatal(err.Error())
	}

	fmt.Println("public key ")
	fmt.Println(hex.EncodeToString(publicKeyBytes))

	encodedPublicKey := "0000100100000010db603876f075bd701bb8debb5afefc210000100200000010db603876f075bd701bb8debb5afefc220000100300000020bbfdc4c5e5120ae1f4cbc08ef2b681c71fa23104e9dd6989b395db767dccfc41"
	encPubKey, _ := hex.DecodeString(encodedPublicKey)
	decodedPubKey, err := publicKeyEncoder.Decode(encPubKey)
	if err != nil{
		log.Fatal(err.Error())
	}
	fmt.Println("decoded pub key : ", MustJSON(decodedPubKey))


	fmt.Println("encode private key")
	privateKeyBytes, err := privateKeyEncoder.Encode(*privKey)

	if err != nil{
		log.Fatal(err.Error())
	}

	fmt.Println("private key ")
	fmt.Println(hex.EncodeToString(privateKeyBytes))

	encodedPrivateKey := "0000100100000010c7ea4dc8e84d81c6b33f43deb3dcf89f0000100200000010c7ea4dc8e84d81c6b33f43deb3dcf8a000001003000000209c1e1b60a413a6de95e4ec27e8a4b89bab3dca4c9528b246d403302e9cbc72c10000100400000010c7ea4dc8e84d81c4ecaddeaf91c5df00000010050000001096076d84af782c7ecded7a1d3f76c4c9"
	encPrivKey, _ := hex.DecodeString(encodedPrivateKey)
	decodedPrivKey, err := privateKeyEncoder.Decode(encPrivKey)
	if err != nil{
		log.Fatal(err.Error())
	}
	fmt.Println("decoded priv key : ", MustJSON(decodedPrivKey))

	/*// Encrypt the number "15".
	m15 := new(big.Int).SetInt64(15)
	c15, _ := paillier.Encrypt(&privKey.PublicKey, m15.Bytes())

	// Decrypt the number "15".
	d, _ := paillier.Decrypt(privKey, c15)
	plainText := new(big.Int).SetBytes(d)
	fmt.Println("Decryption Result of 15: ", plainText.String()) // 15

	// Now for the fun stuff.

	// Encrypt the number "20".
	m20 := new(big.Int).SetInt64(20)
	c20, _ := paillier.Encrypt(&privKey.PublicKey, m20.Bytes())

	// Add the encrypted integers 15 and 20 together.
	plusM16M20 := paillier.AddCipher(&privKey.PublicKey, c15, c20)
	decryptedAddition, _ := paillier.Decrypt(privKey, plusM16M20)
	fmt.Println("Result of 15+20 after decryption: ",
		new(big.Int).SetBytes(decryptedAddition).String()) // 35!

	// Add the encrypted integer 15 to plaintext constant 10.
	plusE15and10 := paillier.Add(&privKey.PublicKey, c15, new(big.Int).SetInt64(10).Bytes())
	decryptedAddition, _ = paillier.Decrypt(privKey, plusE15and10)
	fmt.Println("Result of 15+10 after decryption: ",
		new(big.Int).SetBytes(decryptedAddition).String()) // 25!

	// Multiply the encrypted integer 15 by the plaintext constant 10.
	mulE15and10 := paillier.Mul(&privKey.PublicKey, c15, new(big.Int).SetInt64(10).Bytes())
	decryptedMul, _ := paillier.Decrypt(privKey, mulE15and10)
	fmt.Println("Result of 15*10 after decryption: ",
		new(big.Int).SetBytes(decryptedMul).String()) // 150!*/


}

func MustJSON(v interface{}) string {
	b, err := json.MarshalIndent(v, "", "\t")
	if err != nil {
		return err.Error()
	}
	return string(b)
}