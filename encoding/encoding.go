package encoding

type Encoder interface {
	Encode(interface{}) ([]byte, error)
}

type Decoder interface {
	Decode([]byte) (interface{}, error)
}


type EncoderDecoder interface {
	Encoder
	Decoder
}